console.log('>>> Simple Anonymous Functions');

const hello = function() { console.log('Hello'); }
hello();

const add = function(x, y) { return x + y; }
const sub = function(x, y) { return x - y; }

console.log(`3 + 5 = ${add(3, 5)}`);
console.log(`5 - 3 = ${sub(5, 3)}`);


console.log('>>> Anonymous Functions a Function Parameters');

function printResult(operation) {
    console.log(`Result: ${operation(5, 3)}`);
}

printResult(add);
printResult(sub);
printResult(function(x, y) { return x * y; });
printResult(function(x, y) { return x / y; });
